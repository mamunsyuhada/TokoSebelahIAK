package com.example.mamunsyuhada.tokosebelahiak;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BayarActivityActivity extends AppCompatActivity {

    private static final String TAG = "BayarActivityActivity";
    private int jumlahBarang = 0;
    private RadioGroup radioGrup;
    private int checked;
    private TextView tvJumlahHarga;
    private int hargaOngkir = 0;
    private TextView tvHargaOngkir;
    private TextView tvJumlahSemua;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar_activity);

        final Spinner spinnerWilayah = findViewById(R.id.spinnerWilayah);

        radioGrup = findViewById(R.id.radioBtnGrup);
        radioGrup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                checked = radioGrup.indexOfChild(findViewById(i));
                switch (checked){
                    case 0:
                        int wahanaPos = 2000;
//                        Toast.makeText(getBaseContext(), radioWahanaPos.getText(),Toast.LENGTH_LONG).show();
                        String hasilWilayah = String.valueOf(spinnerWilayah.getSelectedItem());
                        switch (hasilWilayah){
                            case "Yogyakarta":
                                int jasaKirim = 2;
                                hargaOngkir = jasaKirim * wahanaPos;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Bandung":
                                jasaKirim = 4;
                                hargaOngkir = jasaKirim * wahanaPos;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Magelang":
                                jasaKirim = 8;
                                hargaOngkir = jasaKirim * wahanaPos;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Sukoharjo":
                                jasaKirim = 10;
                                hargaOngkir = jasaKirim * wahanaPos;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Pilih Kota Tujuan":
                                jasaKirim = 0;
                                hargaOngkir = jasaKirim * wahanaPos;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                        }
                        break;
                    case 1:
                        int posIndonesia = 2500;
//                        Toast.makeText(getBaseContext(), radioPosIndonesia.getText(), Toast.LENGTH_LONG).show();
                        hasilWilayah = String.valueOf(spinnerWilayah.getSelectedItem());
                        switch (hasilWilayah){
                            case "Yogyakarta":
                                int jasaKirim = 2;
                                hargaOngkir = jasaKirim * posIndonesia;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Bandung":
                                jasaKirim = 4;
                                hargaOngkir = jasaKirim * posIndonesia;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Magelang":
                                jasaKirim = 8;
                                hargaOngkir = jasaKirim * posIndonesia;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Sukoharjo":
                                jasaKirim = 10;
                                hargaOngkir = jasaKirim * posIndonesia;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                            case "Pilih Kota Tujuan":
                                jasaKirim = 0;
                                hargaOngkir = jasaKirim * posIndonesia;
                                tampilkanHargaOngkir(hargaOngkir);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void tampilkanHargaOngkir(int hargaOngkir) {
        tvHargaOngkir = findViewById(R.id.tvHargaPengiriman);
        tvHargaOngkir.setText(String.valueOf(hargaOngkir));
    }

    public void btnAddBarang(View view) {
        jumlahBarang = jumlahBarang + 1;
        tampilkanKeEditText(jumlahBarang);
    }

    public void btnSubtractBarang(View view) {
        jumlahBarang = jumlahBarang - 1;
        tampilkanKeEditText(jumlahBarang);
    }

    private void tampilkanKeEditText(int jumlahBarang) {
        EditText editTextJmlBarang;
        if (jumlahBarang < 0){
            jumlahBarang = 0;
            editTextJmlBarang = findViewById(R.id.editTextJumlahBarang);
            editTextJmlBarang.setText(String.valueOf(jumlahBarang));
            Toast.makeText(this,"Nggak boleh ngutang", Toast.LENGTH_LONG).show();

            tampilkanHargaJumlahBarang(jumlahBarang);
        }else {
            editTextJmlBarang = findViewById(R.id.editTextJumlahBarang);
            editTextJmlBarang.setText(String.valueOf(jumlahBarang));
            tampilkanHargaJumlahBarang(jumlahBarang);
        }
    }

    private void tampilkanHargaJumlahBarang(int jumlahBarang) {
        int hargaNovelDilan = 60000;
        int hargaJumlahBarang = jumlahBarang * hargaNovelDilan;
        tvJumlahHarga = findViewById(R.id.tvJumlahHargaBarang);
        tvJumlahHarga.setText(String.valueOf(hargaJumlahBarang));
        tampilkanTotalHarga(hargaJumlahBarang, hargaOngkir);
    }

    private void tampilkanTotalHarga(int hargaJumlahBarang, int hargaOngkir) {
        int jumlahSemua = hargaJumlahBarang+hargaOngkir;
        tvJumlahSemua = findViewById(R.id.tvTotalBayar);
        tvJumlahSemua.setText(String.valueOf(jumlahSemua));
    }

    public void closeBayarActivity(View view) {
        this.finish();
    }
}